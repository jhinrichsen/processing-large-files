# If possible, all command options are optimized to take existing files into
# account, and only do something if anything changed at all.

JAR ?= readFileJava-0.0.1-SNAPSHOT.jar

DATA := itcont.txt
DATA_SMALL := itcont-10.txt
JAR_PATH := reference/build/libs/$(JAR)

.PHONY: all
all: $(DATA) $(DATA_SMALL) diff

.PHONY: clean
clean:
	rm -rf builds got.txt indiv18.zip $(DATA) reference p? performance-patterns want.txt

indiv18.zip:
	wget -N -nv https://www.fec.gov/files/bulk-downloads/2018/indiv18.zip

$(DATA): | indiv18.zip
	unzip -uo indiv18.zip $(DATA)

$(DATA_SMALL): $(DATA)
	head -n 10 $< >$@
	cat $@

reference/.git/config:
	git clone https://github.com/paigen11/read-file-java reference

$(JAR_PATH): reference/.git/config
	(cd reference && ./gradlew jar)

# remove performance related entries in reference output, those cannot be
# reproduced
performance-patterns:
	echo 'Reading file using .*' > $@
	echo 'Line count time: .*' >> $@
	echo 'Name time: .*' >> $@
	echo 'Donations time: .*' >> $@
	echo 'Most common name time: .*' >> $@

# Donations are not sorted in any way which makes comparing hard, so go for
# sorting
want.txt: $(DATA) $(JAR_PATH) performance-patterns
	java -jar $(JAR_PATH) $(DATA) | grep -v -f performance-patterns | sort > want.txt

got.txt: p1 p2 p3 p4 p5 p6
	echo "Name: $(shell cat p1) at index: 0" > $@
	echo "Name: $(shell cat p2) at index: 432" >> $@
	echo "Name: $(shell cat p3) at index: 43243" >> $@
	echo "Total file line count: $(shell cat p4)" >> $@
	cat p5|awk '{ \
		print "Donations per month and year: " \
		substr($$2, 5, 6) \
		"-" \
		substr($$2, 0, 4) \
		" and donation count: " \
		$$1 \
	}' >> $@
	echo "The most common first name is: $(shell cat p6|tr -s ' '|cut -d' ' -f2) and it occurs: $(shell cat p6|tr -s ' '|cut -d' ' -f1) times." >> $@
	# sort in-place
	sort -o $@ $@

.PHONY: diff
diff: want.txt got.txt
	diff want.txt got.txt

p1 p2 p3 p4 p5 p6: $(DATA)
p1:
	head -1 $(DATA)|cut -d '|' -f 8 >$@

p2:
	head -433 $(DATA)|tail -n 1|cut -d'|' -f8 >$@

p3:
	head -43244 $(DATA)|tail -n 1|cut -d'|' -f8 >$@

p4:
	wc -l $(DATA)|cut -d' ' -f2 >$@

p5:
	cat $(DATA)|cut -d'|' -f5|cut -c -6|sort -n|uniq -c >$@

p6:
	cat $(DATA)|cut -d'|' -f8|cut -d',' -f2|tr -s ' '|cut -d' ' -f2|sort|uniq -c|sort -nr|head -1 >$@
